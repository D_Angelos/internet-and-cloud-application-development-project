# README #

### Set up ###

1. Δημιουργία στη βάση schema με όνομα djbr. 

		CREATE SCHEMA IF NOT EXISTS djbr;
	
2. Data import στην sql τα περιεχόμενα του φακέλου database
3. Ανοίγουμε τον SQL Server μέσω xampp ή άλλου εργαλείου
4. Στο φάκελο client εκτελούμε την εντολή 

		ng build
	
5. Στο φάκελο server εκτελούμε την εντολή 

		python manage.py runserver
	
6. Ανοίγουμε τον browser στο 

		localhost:8000
	

### Χρήση σελίδας ###

* Για μη συνδεδεμένους χρήστες υπάρχει μια λίστα με ταινίες που μπορεί να δει τις πληροφορίες τους
* Για συνδεδεμένους χρήστες υπάρχει η δυνατότηtα να προσθέσουν όσες ταινίες θέλουν στη wislist ή στο καλάθι αγορών, τα οποία μπορούν να επεξεργαστούν στο κατάλληλο tab

### Χρήστες ###

* Django admin

		username: admin password: adminpass
	
* Απλός user

		username: testuser password: testpass
	
