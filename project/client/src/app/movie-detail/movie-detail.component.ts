import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MovieService }  from '../services/movie.service';

import { Movie } from '../models/movie';

import { CartService } from '../services/cart.service';

import { WishlistService } from '../services/wishlist.service';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  @Input()
  movie: Movie;

  public cart: Movie[] = [];
  public wishlist: Movie[] = [];

  constructor(
	  private route: ActivatedRoute,
	  private movieService: MovieService,
	  private location: Location,
    private cartService: CartService,
    private wishlistService: WishlistService,
    private auth: AuthService
  ) { 
    cartService.getItems()
      .subscribe(_ => this.cart = _);
    wishlistService.getItems()
      .subscribe(_ => this.wishlist = _);
  }

  ngOnInit(): void {
	   this.getMovie();
  }

  getMovie(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.movieService.getMovie(id).subscribe(movie => this.movie = movie);
  }
  
  goBack(): void {
    this.location.back();
  }
  
  save(): void {
    this.movieService.updateMovie(this.movie)
      .subscribe(() => this.goBack());
  }

  public addToCart(movie: Movie) {
    this.cartService.addToCart(movie);
  }

  public removeFromCart(movie: Movie) {
    this.cartService.removeFromCart(movie);
  }

  public addToWishlist(movie: Movie) {
    this.wishlistService.addToWishlist(movie);
  }

  public removeFromWishlist(movie: Movie) {
    this.wishlistService.removeFromWishlist(movie);
  }

  private itemIsInCart(item: Movie): boolean {
    return this.cart.find(_ => _.id === item.id) != null;
  }

  private itemIsInWishlist(item: Movie): boolean {
    return this.wishlist.find(_ => _.id === item.id) != null;
  }
}
