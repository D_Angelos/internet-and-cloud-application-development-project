-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: djbr
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-02-11 19:03:06.149193'),(2,'auth','0001_initial','2018-02-11 19:03:10.816103'),(3,'admin','0001_initial','2018-02-11 19:03:12.108100'),(4,'admin','0002_logentry_remove_auto_add','2018-02-11 19:03:12.148846'),(5,'contenttypes','0002_remove_content_type_name','2018-02-11 19:03:12.642394'),(6,'auth','0002_alter_permission_name_max_length','2018-02-11 19:03:13.091762'),(7,'auth','0003_alter_user_email_max_length','2018-02-11 19:03:13.475320'),(8,'auth','0004_alter_user_username_opts','2018-02-11 19:03:13.508443'),(9,'auth','0005_alter_user_last_login_null','2018-02-11 19:03:13.691174'),(10,'auth','0006_require_contenttypes_0002','2018-02-11 19:03:13.709871'),(11,'auth','0007_alter_validators_add_error_messages','2018-02-11 19:03:13.732747'),(12,'auth','0008_alter_user_username_max_length','2018-02-11 19:03:14.292320'),(13,'djbr','0001_initial','2018-02-11 19:03:15.026168'),(14,'djbr','0002_movie_price','2018-02-11 19:03:15.280839'),(15,'djbr','0003_movie_rating','2018-02-11 19:03:15.462997'),(16,'djbr','0004_movie_genre','2018-02-11 19:03:15.654840'),(17,'djbr','0005_auto_20180211_2050','2018-02-11 19:03:17.360252'),(18,'sessions','0001_initial','2018-02-11 19:03:17.674389'),(19,'djbr','0006_author','2018-02-11 19:10:07.392756'),(20,'djbr','0007_auto_20180211_2109','2018-02-11 19:10:08.817689'),(21,'djbr','0008_auto_20180211_2318','2018-02-11 21:18:30.217103'),(22,'djbr','0009_auto_20180212_1231','2018-02-12 10:31:39.340357'),(23,'djbr','0010_remove_movie_direc','2018-02-12 16:11:52.624132'),(24,'djbr','0011_auto_20180212_1819','2018-02-12 16:20:00.554200');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-16 14:59:26
