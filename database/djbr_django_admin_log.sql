-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: djbr
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-02-11 19:14:09.282316','1','Movie object',1,'[{\"added\": {}}]',1,1),(2,'2018-02-11 19:14:42.558658','1','Zack Snyder',1,'[{\"added\": {}}]',9,1),(3,'2018-02-11 20:18:25.529417','2','Star Wars: Episode VIII - The Last Jedi 2017',1,'[{\"added\": {}}]',1,1),(4,'2018-02-11 20:19:08.396627','3','Thor: Ragnarok 2017',1,'[{\"added\": {}}]',1,1),(5,'2018-02-11 20:19:24.684125','2','Rian Johnson',1,'[{\"added\": {}}]',9,1),(6,'2018-02-11 20:19:30.224917','3','Taika Waititi',1,'[{\"added\": {}}]',9,1),(7,'2018-02-12 15:49:24.674414','3','Thor: Ragnarok 2017',2,'[{\"changed\": {\"fields\": [\"direc\", \"poster\"]}}]',1,1),(8,'2018-02-12 15:49:39.989369','2','Star Wars: Episode VIII - The Last Jedi 2017',2,'[{\"changed\": {\"fields\": [\"direc\", \"poster\"]}}]',1,1),(9,'2018-02-12 15:49:53.723151','1','Justice League 2017',2,'[{\"changed\": {\"fields\": [\"direc\", \"poster\"]}}]',1,1),(10,'2018-02-12 16:22:19.837141','1','Justice League 2017',2,'[{\"changed\": {\"fields\": [\"director\"]}}]',1,1),(11,'2018-02-12 16:22:54.804340','2','Star Wars: Episode VIII - The Last Jedi 2017',2,'[{\"changed\": {\"fields\": [\"director\", \"writers\", \"actors\"]}}]',1,1),(12,'2018-02-12 16:23:06.103734','3','Thor: Ragnarok 2017',2,'[{\"changed\": {\"fields\": [\"director\"]}}]',1,1),(13,'2018-02-12 16:58:53.961834','3','Thor: Ragnarok 2017',2,'[{\"changed\": {\"fields\": [\"genre\", \"writers\", \"actors\"]}}]',1,1),(14,'2018-02-12 16:59:15.178760','2','Star Wars: Episode VIII - The Last Jedi 2017',2,'[{\"changed\": {\"fields\": [\"genre\"]}}]',1,1),(15,'2018-02-12 17:00:19.304252','1','Justice League 2017',2,'[{\"changed\": {\"fields\": [\"desc\", \"actors\"]}}]',1,1),(16,'2018-02-12 17:01:07.910926','4','Hacksaw Ridge 2016',1,'[{\"added\": {}}]',1,1),(17,'2018-02-12 17:01:58.594231','5','American Made 2017',1,'[{\"added\": {}}]',1,1),(18,'2018-02-14 17:03:00.789869','6','The Foreigner 2017',1,'[{\"added\": {}}]',1,1),(19,'2018-02-14 17:03:40.357298','7','Dunkirk 2017',1,'[{\"added\": {}}]',1,1),(20,'2018-02-14 17:04:25.748902','8','Baby Driver 2017',1,'[{\"added\": {}}]',1,1),(21,'2018-02-14 17:05:03.699665','9','Spider-Man: Homecoming 2017',1,'[{\"added\": {}}]',1,1),(22,'2018-02-14 17:05:40.741507','10','Transformers: The Last Knight 2017',1,'[{\"added\": {}}]',1,1),(23,'2018-02-14 17:06:19.883476','11','The Snowman 2017',1,'[{\"added\": {}}]',1,1),(24,'2018-02-14 17:07:12.291799','12','Get Out 2017',1,'[{\"added\": {}}]',1,1),(25,'2018-02-14 17:08:11.509058','13','Guardians of the Galaxy Vol. 2 2017',1,'[{\"added\": {}}]',1,1),(26,'2018-02-14 17:08:54.686594','14','Wonder Woman 2017',1,'[{\"added\": {}}]',1,1),(27,'2018-02-14 17:09:38.795095','15','Jurassic World 2015',1,'[{\"added\": {}}]',1,1),(28,'2018-02-16 11:10:23.026333','2','testuser',1,'[{\"added\": {}}]',6,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-16 14:59:25
