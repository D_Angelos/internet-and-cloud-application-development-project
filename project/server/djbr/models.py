from django.db import models
from decimal import Decimal
from django.utils import timezone

class Movie(models.Model):
    title = models.CharField(max_length=200)
    pub_year = models.IntegerField('date published', default=2000)
    rating = models.DecimalField(max_digits=3, decimal_places=1, default=Decimal('7.5'))
    genre = models.CharField(max_length=50, default='')
    duration = models.IntegerField('duration', default=100)
    pg = models.IntegerField('pg', default=13)
    director = models.CharField(max_length=20, default='')
    writers = models.CharField(max_length=100, default='')
    desc = models.CharField(max_length=200, default='')
    actors = models.CharField(max_length=100, default='')
    poster = models.CharField(max_length=100, default='')
    price = models.DecimalField(max_digits=5, decimal_places=2, default=Decimal('10.00'))
    cart = models.CharField(max_length=5, default='false')
    
    def was_published_recently(self):
        return (self.pub_year >= timezone.now().year - 1)

    def __str__(self):
        return "%s %s" % (self.title, self.pub_year)