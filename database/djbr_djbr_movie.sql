-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: djbr
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `djbr_movie`
--

DROP TABLE IF EXISTS `djbr_movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djbr_movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `pub_year` int(11) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `rating` decimal(3,1) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `actors` varchar(100) NOT NULL,
  `desc` varchar(200) NOT NULL,
  `duration` int(11) NOT NULL,
  `pg` int(11) NOT NULL,
  `poster` varchar(100) NOT NULL,
  `writers` varchar(100) NOT NULL,
  `cart` varchar(5) NOT NULL,
  `director` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djbr_movie`
--

LOCK TABLES `djbr_movie` WRITE;
/*!40000 ALTER TABLE `djbr_movie` DISABLE KEYS */;
INSERT INTO `djbr_movie` VALUES (1,'Justice League',2017,15.00,7.2,'Action, Adventure, Fantasy','Ben Affleck, Gal Gadot, Jason Momoa','Fueled by his restored faith in humanity and inspired by Superman\'s selfless act, Bruce Wayne enlists the help of his newfound ally, Diana Prince, to face an even greater enemy.',120,13,'static/posters/Justice-League.jpg','Chris Terrio, Joss Whedon','false','Zack Snyder'),(2,'Star Wars: Episode VIII - The Last Jedi',2017,20.00,8.2,'Action, Adventure, Fantasy','Daisy Ridley, John Boyega, Mark Hamill','Having taken her first steps into the Jedi world, Rey joins Luke Skywalker on an adventure with Leia, Finn and Poe that unlocks mysteries of the Force and secrets of the past.',152,13,'static/posters/Last-Jedi.jpeg','Rian Johnson, George Lucas','false','Rian Johnson'),(3,'Thor: Ragnarok',2017,20.00,8.1,'Action, Adventure, Comedy','Chris Hemsworth, Tom Hiddleston, Cate Blanchett','Imprisoned, the almighty Thor finds himself in a lethal gladiatorial contest against the Hulk, his former ally. Thor must fight for survival and race against time to prevent the all-powerful Hela from',130,13,'static/posters/Thor-Ragnarok.jpg','Eric Pearson, Craig Kyle','false','Taika Waititi'),(4,'Hacksaw Ridge',2016,10.00,8.2,'Biography, Drama, History','Andrew Garfield, Sam Worthington, Luke Bracey','WWII American Army Medic Desmond T. Doss, who served during the Battle of Okinawa, refuses to kill people, and becomes the first man in American history to receive the Medal of Honor without firing a',139,18,'static/posters/Hacksaw-Ridge.jpg','Robert Schenkkan, Andrew Knight','false','Mel Gibson'),(5,'American Made',2017,10.00,7.2,'Action, Biography, Comedy','Tom Cruise, Domhnall Gleeson, Sarah Wright','The story of Barry Seal, an American pilot who became a drug-runner for the CIA in the 1980s in a clandestine operation that would be exposed as the Iran-Contra Affair.',115,16,'static/posters/American-Made.jpg','Gary Spinelli','false','Doug Liman'),(6,'The Foreigner',2017,10.00,7.1,'Action, Crime, Drama','Katie Leung, Jackie Chan, Rufus Jones','A humble businessman with a buried past seeks justice when his daughter is killed in an act of terrorism. A cat-and-mouse conflict ensues with a government official, whose past may hold clues to the k',113,16,'static/posters/Foreigner.jpg','David Marconi,  Stephen Leather','false','Martin Campbell'),(7,'Dunkirk',2017,15.00,8.1,'Action, Drama, History','Fionn Whitehead, Barry Keoghan, Mark Rylance','Allied soldiers from Belgium, the British Empire and France are surrounded by the German Army, and evacuated during a fierce battle in World War II.',96,16,'static/posters/Dunkirk.jpg','Christopher Nolan','false','Christopher Nolan'),(8,'Baby Driver',2017,10.00,7.7,'Action, Crime, Music','Ansel Elgort, Jon Bernthal, Jon Hamm','After being coerced into working for a crime boss, a young getaway driver finds himself taking part in a heist doomed to fail.',112,16,'static/posters/Baby-Driver.jpg','Edgar Wright','false','Edgar Wright'),(9,'Spider-Man: Homecoming',2017,20.00,7.6,'Action, Adventure, Sci-Fi','Tom Holland, Michael Keaton, Robert Downey Jr.','Peter Parker balances his life as an ordinary high school student in Queens with his superhero alter-ego Spider-Man, and finds himself on the trail of a new menace prowling the skies of New York City.',133,13,'static/posters/Spider-Man.jpg','Jonathan Goldstein, John Francis Daley','false','Taika Waititi'),(10,'Transformers: The Last Knight',2017,7.00,5.2,'Action, Adventure, Sci-Fi','Mark Wahlberg, Anthony Hopkins, Josh Duhamel','Autobots and Decepticons are at war, with humans on the sidelines. Optimus Prime is gone. The key to saving our future lies buried in the secrets of the past, in the hidden history of Transformers on',155,13,'static/posters/Transformers.jpg','Art Marcum, Matt Holloway','false','Michael Bay'),(11,'The Snowman',2017,7.00,5.2,'Crime, Drama, Horror','Michael Fassbender, Rebecca Ferguson, Charlotte Gainsbourg','Detective Harry Hole investigates the disappearance of a woman whose scarf is found wrapped around an ominous-looking snowman.',119,18,'static/posters/Snowman.jpg','Peter Straughan, Hossein Amini','false','Tomas Alfredson'),(12,'Get Out',2017,10.00,7.7,'Horror, Mystery, Thriller','Daniel Kaluuya, Allison Williams, Bradley Whitford','It\'s time for a young African-American to meet with his white girlfriend&#39;s parents for a weekend in their secluded estate in the woods, but before long, the friendly and polite ambience will give',104,18,'static/posters/Get-Out.jpg','Jordan Peele','false','Jordan Peele'),(13,'Guardians of the Galaxy Vol. 2',2017,15.00,7.8,'Action, Adventure, Sci-Fi','Chris Pratt, Zoe Saldana, Dave Bautista','he Guardians must fight to keep their newfound family together as they unravel the mystery of Peter Quill\'s true parentage.',136,13,'static/posters/Guardians.jpg','James Gunn,Dan Abnett','false','James Gunn'),(14,'Wonder Woman',2017,20.00,7.6,'Action, Adventure, Fantasy','Gal Gadot, Chris Pine, Robin Wright','When a pilot crashes and tells of conflict in the outside world, Diana, an Amazonian warrior in training, leaves home to fight a war, discovering her full powers and true destiny.',141,13,'static/posters/Wonder-Woman.jpg','Allan Heinberg,Zack Snyder','false','Patty Jenkins'),(15,'Jurassic World',2015,10.00,7.0,'Action, Adventure, Sci-Fi','Chris Pratt, Bryce Dallas Howard, Ty Simpkins','A new theme park, built on the original site of Jurassic Park, creates a genetically modified hybrid dinosaur, which escapes containment and goes on a killing spree.',124,13,'static/posters/Jurassic.jpg','Rick Jaffa,Amanda Silver','false','Colin Trevorrow');
/*!40000 ALTER TABLE `djbr_movie` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-16 14:59:26
