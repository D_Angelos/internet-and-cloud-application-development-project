import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';

import { Movie } from './../models/movie';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class MovieService {
	
  private moviesUrl = 'api/movies';

  /** GET movies from the server */
  getMovies(): Observable<Movie[]> {
	return this.http.get<Movie[]>(this.moviesUrl)
   .pipe(
     tap(movies => this.log(`fetched movies`)),
     catchError(this.handleError('getMovies', []))
   );
}
  
  getMovie(id: number): Observable<Movie> {
    const url = `${this.moviesUrl}/${id}`;
    return this.http.get<Movie>(url).pipe(
      tap(_ => this.log(`fetched movie id=${id}`)),
      catchError(this.handleError<Movie>(`getMovie id=${id}`))
    );
  }
  
  /** PUT: update the movie on the server */
	updateMovie (movie: Movie): Observable<any> {
	  return this.http.put(this.moviesUrl, movie, httpOptions).pipe(
		tap(_ => this.log(`updated movie id=${movie.id}`)),
		catchError(this.handleError<any>('updateMovie'))
	  );
	}
	
	/** POST: add a new movie to the server */
	addMovie (movie: Movie): Observable<Movie> {
	  return this.http.post<Movie>(this.moviesUrl, movie, httpOptions).pipe(
		tap((movie: Movie) => this.log(`added movie w/ id=${movie.id}`)),
		catchError(this.handleError<Movie>('addMovie'))
	  );
	}
	
	/** DELETE: delete the movie from the server */
	deleteMovie(movie: Movie | number): Observable<Movie> {
	  const id = typeof movie === 'number' ? movie : movie.id;
	  const url = `${this.moviesUrl}/${id}`;

	  return this.http.delete<Movie>(url, httpOptions).pipe(
		tap(_ => this.log(`deleted movie id=${id}`)),
		catchError(this.handleError<Movie>('deleteMovie'))
	  );
	}
	
	/* GET movies whose title contains search term */
	searchMovies(term: string): Observable<Movie[]> {
	  if (!term.trim()) {
		// if not search term, return empty movie array.
		return of([]);
	  }
	  return this.http.get<Movie[]>(`api/movies/?title=${term}`).pipe(
		tap(_ => this.log(`found movies matching "${term}"`)),
		catchError(this.handleError<Movie[]>('searchMovies', []))
	  );
	}
  
  constructor(private http: HttpClient,
			  private messageService: MessageService) { }
			  
  private log(message: string): void {
    this.messageService.add('MovieService: ' + message);
  }
  
  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
