from rest_framework import serializers
from .models import Movie

class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('id', 'title', 'pub_year', 'rating', 'genre', 'duration', 'pg', 'director', 'writers', 'desc', 'actors', 'poster', 'price', 'cart')