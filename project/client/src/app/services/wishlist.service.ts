import { Injectable } from '@angular/core';
import {Movie} from './../models/movie';
import {BehaviorSubject, Observable, Subject, Subscriber} from 'rxjs';
import {of} from 'rxjs/observable/of';

@Injectable()
export class WishlistService {
  private itemsInWishlistSubject: BehaviorSubject<Movie[]> = new BehaviorSubject([]);
  private itemsInWishlist: Movie[] = [];

  constructor() {
  	this.itemsInWishlistSubject.subscribe(_ => this.itemsInWishlist = _);
  }

  public addToWishlist(item: Movie) {
    this.itemsInWishlistSubject.next([...this.itemsInWishlist, item]);
  }

  public getItems(): Observable<Movie[]> {
    return this.itemsInWishlistSubject;
  }

  public getTotalAmount(): Observable<number> {
    return this.itemsInWishlistSubject.map((items: Movie[]) => {
      return items.reduce((prev, curr: Movie) => {
        return prev + curr.price;
      }, 0);
    });
  }

  public removeFromWishlist(item: Movie) {
    const currentItems = [...this.itemsInWishlist];
    const itemsWithoutRemoved = currentItems.filter(_ => _.id !== item.id);
    this.itemsInWishlistSubject.next(itemsWithoutRemoved);
  }

}
