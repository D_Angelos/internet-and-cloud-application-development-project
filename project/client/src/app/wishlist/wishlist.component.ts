import { Component, OnInit } from '@angular/core';
import {WishlistService} from './../services/wishlist.service';
import {Movie} from '../models/movie';
import {Observable} from 'rxjs';
import {of} from 'rxjs/observable/of';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {

  public wishlistItems$: Observable<Movie[]> = of([]);
  public wishlistItems: Movie[] = [];
  p: number = 1;

  constructor(private wishlistService: WishlistService) {
  	this.wishlistItems$ = this
      .wishlistService
      .getItems();

    this.wishlistItems$.subscribe(_ => this.wishlistItems = _);
  }

  ngOnInit() {
  }

  public getTotal(): Observable<number> {
    return this.wishlistService.getTotalAmount();
  }

  public removeItem(item: Movie) {
    this.wishlistService.removeFromWishlist(item)
  }

}
