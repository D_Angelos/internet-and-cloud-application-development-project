from django.conf.urls import url
from . import views

app_name = 'djbr'

urlpatterns = [
    url(r'^movies/?$', views.MovieList.as_view()),
    url(r'^movies/(?P<pk>[0-9]+)/?$', views.MovieDetail.as_view()),
]