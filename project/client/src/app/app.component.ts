import { Component, OnInit } from '@angular/core';

import { AuthService } from './services/auth.service';

import { Movie } from './models/movie';
import { MovieService } from './services/movie.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Movie Store';

  movies: Movie[] = [];

  constructor(private movieService: MovieService, private auth: AuthService) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies(): void {
    this.movieService.getMovies()
      .subscribe(movies => this.movies = movies.slice(1, 5));
  }

}