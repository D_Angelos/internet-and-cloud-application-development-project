export class Movie {
  id: number;
  title: string;
  pub_year: number;
  rating: number;
  genre: String[];
  duration: number;
  pg: number;
  director: string;
  writers: String[];
  desc: string;
  actors: String[];
  poster: string;
  price: number;
  cart: boolean;
}