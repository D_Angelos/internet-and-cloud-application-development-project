import { AlertModule } from 'ngx-bootstrap';

import { BrowserModule } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule }    from '@angular/common/http';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './token.interceptor';

import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { ItalicsDirective } from './italics.directive';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';

import { MovieService } from './services/movie.service';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './services/message.service';
import { ErrorHandlingService } from './services/errorhandling.service';
import { AppRoutingModule } from './app-routing.module';
import { MovieSearchComponent } from './movie-search/movie-search.component';

import { WishlistComponent } from './wishlist/wishlist.component';
import { WishlistService } from './services/wishlist.service';

import { RouterModule, Routes } from '@angular/router';

import { CartComponent } from './cart/cart.component';
import { CartService } from './services/cart.service';
import { LoginComponent } from './login/login.component';
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    ItalicsDirective,
    MovieDetailComponent,
    MessagesComponent,
  	MovieSearchComponent,
  	WishlistComponent,
  	CartComponent,
  	LoginComponent,
  ],
  imports: [
    AlertModule.forRoot(),
    BrowserModule,
    NgxPaginationModule,
  	FormsModule,
  	AppRoutingModule,
  	HttpClientModule,
    RouterModule.forRoot([
      {path: 'wishlist', component: WishlistComponent},
      {path: 'shoppingcart', component: CartComponent}
    ])
  ],
  providers: [MovieService, MessageService, CartService, 
  WishlistService,
  ErrorHandlingService,
  AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
