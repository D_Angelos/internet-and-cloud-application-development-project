import {Injectable} from '@angular/core';
import {Movie} from './../models/movie';
import {BehaviorSubject, Observable, Subject, Subscriber} from 'rxjs';
import {of} from 'rxjs/observable/of';

@Injectable()
export class CartService {
  private itemsInCartSubject: BehaviorSubject<Movie[]> = new BehaviorSubject([]);
  private itemsInCart: Movie[] = [];

  constructor() {
    this.itemsInCartSubject.subscribe(_ => this.itemsInCart = _);
  }

  public addToCart(item: Movie) {
    this.itemsInCartSubject.next([...this.itemsInCart, item]);
  }

  public getItems(): Observable<Movie[]> {
    return this.itemsInCartSubject;
  }

  public getTotalAmount(): Observable<number> {
    return this.itemsInCartSubject.map((items: Movie[]) => {
      return items.reduce((prev, curr: Movie) => {
        return Number(prev) + Number(curr.price);
      }, 0);
    });
  }

  public removeFromCart(item: Movie) {
    const currentItems = [...this.itemsInCart];
    const itemsWithoutRemoved = currentItems.filter(_ => _.id !== item.id);
    this.itemsInCartSubject.next(itemsWithoutRemoved);
  }

}