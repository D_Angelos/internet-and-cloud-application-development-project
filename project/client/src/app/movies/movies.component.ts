import { Component, OnInit } from '@angular/core';

import { Movie } from '../models/movie';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies: Movie[];
  p: number = 1;

  constructor(private movieService: MovieService) { }

  ngOnInit() {
	  this.getMovies();
  }

  getMovies(): void {
    this.movieService.getMovies()
    .subscribe(movies => this.movies = movies);
  }
  
  add(title: string, pubYearStr: string): void {
  title = title.trim();
  let pub_year = +pubYearStr;
  if (!title || !pub_year) { return; }
  this.movieService.addMovie({ title, pub_year } as Movie)
    .subscribe(movie => {
      this.movies.push(movie);
    });
  }

  delete(movie: Movie): void {
    this.movies = this.movies.filter(h => h !== movie);
    this.movieService.deleteMovie(movie).subscribe();
  }

}
